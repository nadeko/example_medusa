﻿using Nadeko.Snake;
using NadekoBot;
using Discord;

public sealed class MySnek : Snek
{
    
    // a) A simple command, replies to ".hello" with "Hello everyone!"
    [cmd]
    public async Task Hello(AnyContext ctx)
    {
        await ctx.Channel.SendMessageAsync($"Hello everyone!");
    }

    // b) A command to which you can pass a user mention
    // ex: .hello @User
    // output: "Hello User#1234, from res.yml!"
    // note: check res.yml for 'hello' string
    //       the bot will reply with a localized string "hello" and replace {0} in that string with User#1234
    [cmd]
    public async Task Hello(AnyContext ctx, [leftover] IUser target)
    {
        await ctx.ConfirmLocalizedAsync("hello", target);
    }
    
    // a command with a custom type (custom parsing of input into an object is performed by implementing ParamParser)
    // ex: .info John,24
    // output: "Employee John is 24 years old"
    // note: EmployeeInfo hold the data, the data is parsed from input via the EmployeeInfoParser class. 
    //       Parser class must inherit from ParamParser<T>.
    [cmd]
    public async Task Info(AnyContext ctx, [leftover] EmployeeInfo data)
    {
        await ctx.Channel.SendMessageAsync($"Employee {data.Name} is {data.Age} years old.");
    }
}

public class EmployeeInfo
{
    public string Name { get; init; }
    public int Age { get; init; }
}

public class EmployeeInfoParser : ParamParser<EmployeeInfo>
{
    public override ValueTask<ParseResult<EmployeeInfo>> TryParseAsync(AnyContext ctx, string input)
    {
        var parts = input.Split(','); // split input on ','
        
        if (parts.Length == 2 // means data looks like X,Y 
            && !string.IsNullOrWhiteSpace(parts[0]) // X must have at least one character
            && int.TryParse(parts[1], out var age)) // Y must be a number 
        {
            return new(ParseResult<EmployeeInfo>.Success(new()
            {
                Age = age,
                Name = parts[0]
            }));
        }

        return new(ParseResult<EmployeeInfo>.Fail());
    }
}

// 